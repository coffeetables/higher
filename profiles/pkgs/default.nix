{
  config,
  pkgs,
  ...
}: {
  environment.systemPackages = with pkgs; [
    binutils
    curl
    atool
    git
    gotop
    ffmpeg
    mpv
    ripgrep
    fd
  ];
}
