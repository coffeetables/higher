{
  self,
  config,
  pkgs,
  inputs,
  ...
}: {
  age.secrets.rightleftspin.file = "${self}/secrets/rightleftspin.age";
  age.secrets.keepass_main.file = "${self}/secrets/keepass_main.age";

  users.mutableUsers = false;
  users.users.rightleftspin = {
    createHome = true;
    home = "/home/rightleftspin";
    extraGroups = ["wheel" "networkmanager" "ydotool"];
    isNormalUser = true;
    hashedPasswordFile = "/run/agenix/rightleftspin";
  };
  home-manager.users.rightleftspin = {
    imports = self.suites.hm; #++ inputs.scientific-fhs.nixosModules.default;
  };
}
