{
  config,
  pkgs,
  ...
}: {
  boot.resumeDevice = "/dev/disk/by-uuid/0d5de940-a111-4f6d-83f7-0094de1819da";
  boot.kernelParams = ["mem_sleep_default=deep" "resume_offset=12550144"];

  # Suspend-then-hibernate on lid switch
  services.logind = {
    lidSwitch = "suspend-then-hibernate";
    lidSwitchExternalPower = "ignore";
  };

  systemd.sleep.extraConfig = "HibernateDelaySec=2m";

  services.thermald.enable = true;

  services.auto-cpufreq.enable = true;
}
