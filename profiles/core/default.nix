{
  config,
  pkgs,
  ...
}: {
  # Printing Setup
  services.avahi = {
    enable = true;
    nssmdns4 = true;
    openFirewall = true;
  };
  services.printing = {
    enable = true;
    drivers = with pkgs; [gutenprint gutenprintBin brgenml1lpr brgenml1cupswrapper brlaser hplip hplipWithPlugin];
  };

  # Hardware Update Manager
  services.fwupd.enable = true;

  # Nix Package Manager settings
  nix = {
    settings.trusted-users = ["root" "@wheel"];
    optimise.automatic = true;
    extraOptions = ''
      keep-outputs = true
      keep-derivations = true
      fallback = true
      experimental-features = nix-command flakes
    '';
  };

  # Various random settings that are also useful
  time.timeZone = "Canada/Eastern";
  services.earlyoom.enable = true;
}
