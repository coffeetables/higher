{
  config,
  pkgs,
  lib,
  ...
}: {
  home.packages = with pkgs; [
    fuzzel_as_dmenu
    fuzzel_as_rofi

    networkmanager_dmenu

    keepmenu
    wl-clipboard

    # Env variable for dmenu-bluetooth is set systemwide in profiles/niri
    dmenu-bluetooth

    libqalculate
  ];

  home.sessionVariables.DMENU_BLUETOOTH_LAUNCHER = "fuzzel";

  home.file.".local/share/fuzzel_calc" = {
    enable = true;
    source = ./fuzzel_calc;
    executable = true;
  };

  home.file.".local/share/fuzzel_filebrowser" = {
    enable = true;
    source = ./fuzzel_filebrowser;
    executable = true;
  };

  xdg.configFile."keepmenu.ini".source = ./keepmenu.ini;

  programs.fuzzel = {
    enable = true;
  };
}
