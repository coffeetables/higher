;; custom stylix-theme package set in
;; programs.doom-emacs.extraPackages
(use-package! stylix-theme)

;; since ui/doom module isn't enabled in favor of stylix-theme
;; this adds the extra config from ui/doom
;;;###package pos-tip
(setq pos-tip-internal-border-width 6
      pos-tip-border-width 1)

(use-package! solaire-mode
  :hook (doom-load-theme . solaire-global-mode)
  :hook (+popup-buffer-mode . turn-on-solaire-mode))

;; (setq notes-directory (file-truename "~/Documents/Mime"))

;; https://github.com/nix-community/nix-doom-emacs/issues/88
;; (add-hook! 'emacs-startup-hook #'doom-init-ui-h)

;; (setq! bibtex-completion-bibliography (list (concat notes-directory "/references.bib")))

;; (set-formatter! 'alejandra "alejandra --quiet" :modes '(nix-mode))
;;
;; Package Initialization

(use-package! org-noter
  :bind
    (:map org-noter-doc-mode-map
      ("C-i" . org-noter-insert-note)))

(use-package! org-noter
  :custom
    (org-noter-always-create-frame nil))

(add-hook! org-mode
  (yas-activate-extra-mode 'latex-mode)
  (show-smartparens-mode)
  (evil-tex-mode))

(use-package! sage-shell-mode)
(use-package! ob-sagemath)

(use-package! engrave-faces)

(use-package! ement)


;; (use-package! edraw-org
;;   :hook
;;     (org-mode . edraw-org-setup-default)
;;     (org-mode . edraw-org-link-image-mode))
;;
;; (use-package! edraw
;;   :bind
;;     (:map edraw-editor-map
;;       ("u" . edraw-editor-undo)
;;       ("C-r" . edraw-editor-redo)
;;       ("z" . edraw-org-link-finish-edit)
;;       ("y" . edraw-editor-copy-selected-shapes)
;;       ("p" . edraw-editor-paste-and-select)
;;       ("x" . edraw-editor-cut-selected-shapes))
;;   :config
;;     (setq edraw-default-document-properties
;;       '((width . 560) (height . 420) (background . "rgb(0,0,0,0)"))))

(use-package! yasnippet
  :custom
  (yas-triggers-in-field t))

(use-package! envrc
  :ensure t
  :hook (agenix-pre-mode . envrc-mode))

;; (use-package! org-roam
;;   :custom
;;   (org-roam-directory notes-directory))

(use-package! org
  :custom
    (org-agenda-files '("~/Nextcloud/Areas/Planning")))
;;
;; (use-package! webkit)
;; (use-package! webkit-ace)
;; (use-package! evil-collection-webkit
;;   :config
;;   (evil-collection-xwidget-setup))



;; ========
;; Settings
;; ========

;; Ob-sagemath supports only evaluating with a session.
(setq org-babel-default-header-args:sage '((:session . t)
                                           (:results . "output")))

;; C-c c for asynchronous evaluating (only for SageMath code blocks).

;; Do not confirm before evaluation
(setq org-confirm-babel-evaluate nil)

;; Show images when opening a file.
(setq org-startup-with-inline-images t)

(setq! org-latex-listings 'engraved)

(setq cdlatex-math-symbol-alist
  '(
     (?. ("\\cdot" "\\cdots" "\\dots"))
   )
  )

;; add mla paper document class to org
(after! org-mode
  add-to-list 'org-latex-classes
         '("mla"
       "\\documentclass[mla8]{mla}"
       ("\\section{%s}" . "\\section*{%s}")
       ("\\subsection{%s}" . "\\subsection*{%s}")
       ("\\subsubsection{%s}" . "\\subsubsection*{%s}")))

;; ============
;; Key bindings
;; ============
(map! :mode org-mode
  :localleader
  "se" #'org-decrypt-entry
  "sE" #'org-encrypt-entry)

(map! :after cdlatex
  :map cdlatex-mode-map
  :i "TAB" #'cdlatex-tab)


;; =====
;; Hooks
;; =====

;; Show images after evaluating code blocks.
(add-hook 'org-babel-after-execute-hook 'org-display-inline-images)

(add-hook! 'org-mode-hook #'xenops-mode)

(add-hook! 'latex-mode-hook #'xenops-mode)
(add-hook! 'LaTeX-mode-hook #'xenops-mode)

(map! :after org-mode
  :map org-mode-map
  "C-c c" #'ob-sagemath-execute-async)


;; ================
;; Custom Functions
;; ================
;; (defun org-roam-export-all ()
;;   "Re-exports all Org-roam files to Hugo markdown."
;;   (interactive)
;;   (dolist (f (org-roam-list-files))
;;     (with-current-buffer (find-file f)
;;       (when (s-contains? ":PUBLISH:  t" (buffer-string))
;;         (org-hugo-export-wim-to-md)))))

;; pulled from latest org-mode commit To preserve relative bibtex file paths
;; can be removed once org-mode can be updated without issue mentioned in packages.el
(defun org-cite-list-bibliography-files ()
    "List all bibliography files defined in the buffer."
    (delete-dups
     (append (mapcar (lambda (value)
  		     (pcase value
  		       (`(,f . ,d)
                          (setq f (org-strip-quotes f))
                          (if (or (file-name-absolute-p f)
                                  (file-remote-p f)
                                  (equal d default-directory))
                              ;; Keep absolute paths, remote paths, and
                              ;; local relative paths.
                              f
                            ;; Adjust relative bibliography path for
                            ;; #+SETUP files located in other directory.
                            ;; Also, see `org-export--update-included-link'.
                            (file-relative-name
                             (expand-file-name f d) default-directory)))))
  		   (pcase (org-collect-keywords
                             '("BIBLIOGRAPHY") nil '("BIBLIOGRAPHY"))
  		     (`(("BIBLIOGRAPHY" . ,pairs)) pairs)))
  	   org-cite-global-bibliography)))

;; (defun nix-flake-system-attribute-names (types)
;;   "Return a list of output attributes of particular TYPES."
;;   (let ((system (intern (nix-system))))
;;     (thread-last nix-flake-outputs
;;       (mapcar (pcase-lambda (`(,type . ,alist))
;; 		(when (memq type types)
;; 		  (mapcar #'car (alist-get system alist)))))
;;       (apply #'append)
;;       (cl-remove-duplicates)
;;       (mapcar #'symbol-name))))
;;
;;
;; (cl-defun nix-flake--set-flake (flake-ref &key remote)
;;   "Set the flake of the transient interface.
;; FLAKE-REF and REMOTE should be passed down from `nix-flake-dispatch'."
;;   (setq nix-flake-ref flake-ref)
;;   (setq nix-flake-outputs
;;         (if remote
;; 	    (nix--process-json "flake" "show" "--json" nix-flake-ref)
;; 	  (let ((default-directory flake-ref))
;;             (nix--process-json "flake" "show" "--json")))))
;;
