;; (package! edraw)
;; (package! webkit
;;          :recipe (:files (:defaults "*.js" "*.css" "*.so")
;;          ))
(package! org-roam-bibtex)
(package! org-ref)
(package! ob-sagemath)

;; workaround for https://github.com/doomemacs/doomemacs/issues/7437
(package! org :pin "806abc5a2bbcb5f884467a0145547221ba09eb59")
(package! engrave-faces)

;; When using org-roam via the `+roam` flag
;; (unpin! org-roam)

;; When using bibtex-completion via the `biblio` module
;; (unpin! bibtex-completion helm-bibtex ivy-bibtex)

(package! xenops)

(package! docopt)
(package! cue-mode)
(package! pdf-tools :built-in 'prefer)

(package! sage-shell-mode)

(package! ement)

(package! agenix)
