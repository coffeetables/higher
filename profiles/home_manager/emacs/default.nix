{
  pkgs,
  config,
  ...
}: {

  home.packages = [ pkgs.ispell ];
  programs.doom-emacs = {
    enable = true;
    doomDir = ./doom.d;
    emacs = pkgs.emacs29-pgtk;
    # Get theme package from stylix
    extraPackages = epkgs: let
      extraPackages = config.programs.emacs.extraPackages epkgs;
    in
      extraPackages
      ++ [
        (epkgs.trivialBuild {
          pname = "stylix-theme";
          # add config from doom's ui/doom module
          src = pkgs.writeText "stylix-theme.el" config.programs.emacs.extraConfig;
          version = "0.1.0";
          packageRequires = extraPackages;
        })
      ];
  };
}
