{
  config,
  pkgs,
  ...
}: {
  stylix.targets.firefox.profileNames = ["default"];

  programs.firefox = {
    enable = true;
    package = pkgs.firefox.override {
      cfg.drmSupport = true;
    };
    profiles.default = {
      extensions = with pkgs.nur.repos.rycee.firefox-addons; [
        ublock-origin
      ];

      settings = {
        "toolkit.legacyUserProfileCustomizations.stylesheets" = true;
        "browser.uidensity" = 0;
        "svg.content-properties.content.enabled" = true;
      };
    };
  };
}
