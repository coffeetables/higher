{
  config,
  pkgs,
  ...
}: {
  programs = {
    direnv.enable = true;
    home-manager.enable = true;
  };

  home.packages = with pkgs; [
    # Only really has packages for personal use
    rnote
    sage
    vesktop
    keepassxc
    beeper
    zotero
    prismlauncher
    evince
    kdenlive
    amberol
  ];

  home.stateVersion = "24.05";
}
