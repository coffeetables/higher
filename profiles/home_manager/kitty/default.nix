{
  config,
  pkgs,
  ...
}: {
  home.packages = with pkgs; [
    tree
    trash-cli
    busybox
  ];

  programs = {
    kitty = {
      enable = true;
      settings = {
        hide_window_decorations = "yes";
      };
    };
    starship = {
      enable = true;
      settings = {
        format = ''[](#${config.lib.stylix.colors.base03})$username[](bg:#${config.lib.stylix.colors.base02} fg:#${config.lib.stylix.colors.base03})$directory[](fg:#${config.lib.stylix.colors.base02} bg:#${config.lib.stylix.colors.base01})$time[ ](fg:#${config.lib.stylix.colors.base01})'';
        command_timeout = 5000;

        username = {
          show_always = true;
          style_user = "bg:#${config.lib.stylix.colors.base03}";
          style_root = "bg:#${config.lib.stylix.colors.base03}";
          format = "[ ]($style)";
        };
        directory = {
          style = "bg:#${config.lib.stylix.colors.base02}";
          format = "[ $path ]($style)";
          truncation_length = 3;
          truncation_symbol = "…/";
        };
        directory.substitutions = {
          Documents = "󰈙 ";
          Downloads = " ";
          Music = " ";
          Pictures = " ";
        };
        time = {
          disabled = false;
          time_format = "%R"; # Hour:Minute Format
          style = "bg:#${config.lib.stylix.colors.base01}";
          format = "[ $time ]($style)";
        };
      };
    };
    bash = {
      enable = true;
      enableCompletion = true;
      bashrcExtra = builtins.readFile ./bashrc.sh;
    };
  };
}
