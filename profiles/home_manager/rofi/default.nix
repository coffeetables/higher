{
  config,
  pkgs,
  lib,
  ...
}: {
  home.packages = with pkgs; [
    rofi-bluetooth
    rofi-calc
    rofi-file-browser
  ];

  programs.rofi = {
    enable = true;
    package = pkgs.rofi-wayland;
    #   theme = lib.mkAfter ''
    #       window {
    #           location:       center;
    #           width:          480;
    #           border-radius:  24px;
    #       }
    #   '';
    plugins = with pkgs; [rofi-calc rofi-file-browser keepmenu];
  };
}
