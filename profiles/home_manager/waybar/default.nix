{
  config,
  lib,
  pkgs,
  ...
}: {
  systemd.user.targets.tray = {
    Unit = {
      Description = "Home Manager System Tray";
      Requires = ["graphical-session-pre.target"];
      # Any service starting after tray.target also
      # needs to start after "graphical-session.target"
      # to prevent cyclic dependency
      After = ["graphical-session.target"];
    };
    Install.WantedBy = ["graphical-session.target"];
  };

  systemd.user.services.waybar = {
    Unit = {
      After = lib.mkForce ["graphical-session.target"];
      Requisite = ["graphical-session.target"];
    };
    Service = {
      # Ensure bar is actually configured before systemd considers this service started
      ExecStartPost = "${pkgs.coreutils}/bin/sleep 2";
    };
  };

  stylix.targets.waybar = {
    enableCenterBackColors = true;
    enableLeftBackColors = true;
    enableRightBackColors = true;
  };

  programs.waybar = {
    enable = true;
    systemd = {
      enable = true;
      target = "tray.target";
    };

    style = lib.mkAfter ''
      window#waybar {
          background: transparent;
      }

      * {
         padding-right: 2px;
         padding-left: 2px;
         margin-right: 2px;
         margin-left: 2px;

         border-radius: 10px;
      }

      #custom-mem {
          color: #${config.lib.stylix.colors.base00};
          background: #${config.lib.stylix.colors.base03};
      }

      #tray {
          color: #${config.lib.stylix.colors.base00};
          background: #${config.lib.stylix.colors.base03};
      }
    '';
    settings = lib.importJSON ./config/config.json;
  };

  xdg.configFile."waybar/scripts/" = {
    source = ./config/scripts;
    recursive = true;
  };
}
