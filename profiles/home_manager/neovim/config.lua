local dap = require("dap")
dap.adapters.gdb = {
  type = "executable",
  command = "gdb",
  args = { "-i", "dap" }
}

dap.configurations.rust = {
  {
    name = "Launch",
    type = "gdb",
    request = "launch",
    program = function()
      return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
    end,
    cwd = "${workspaceFolder}",
  },
}

dap.configurations.python = {
  {
    name = "Launch",
    type = "gdb",
    request = "launch",
    program = function()
      return vim.fn.input('Path to executable: ', '--args python ', vim.fn.getcwd() .. '/', 'file')
    end,
    cwd = "${workspaceFolder}",
  },
}

require('dapui').setup()
vim.keymap.set('n', '<F9>', '<Cmd> lua require("dapui").toggle()<CR>')
vim.keymap.set('i', '<Tab>', 'coc#pum#visible() ? coc#pum#next(1) : "\\<Tab>"', {expr = true, noremap = true})

vim.opt.spelllang = 'en_us'
vim.opt.spell = true


