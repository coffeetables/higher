{
  config,
  pkgs,
  ...
}: {
  home.packages = with pkgs; [
    rust-analyzer
    nixd
    ruff-lsp
    texlab
    gdb
    languagetool
  ];

  programs.neovim = {
    enable = true;
    defaultEditor = true;
    extraLuaConfig = builtins.readFile ./config.lua;
    plugins = with pkgs.vimPlugins; [
      nvim-dap-ui
      nvim-dap-python
      nvim-dap
      pkgs.ruff-lsp
      coc-rust-analyzer
      pkgs.nixd
      airline
      coc-texlab
      vim-grammarous
      julia-vim
    ];
    coc = {
      enable = true;
      settings = {
        languageserver = {
          rust = {
            command = "rust-analyzer";
            filetypes = ["rust"];
            rootPatterns = ["Cargo.toml"];
          };
          python = {
            command = "ruff-lsp";
            filetypes = ["python"];
          };
          nixd = {
            command = "nixd";
            rootPatterns = [".nixd.json"];
            filetypes = ["nix"];
          };
          latex = {
            command = "texlab";
            filetypes = ["tex" "bib" "plaintex" "context"];
          };
        };
      };
    };
  };
}
