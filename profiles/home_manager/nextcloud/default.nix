{
  config,
  pkgs,
  ...
}: {
  services.nextcloud-client = {
    enable = true;
    startInBackground = true;
  };

  systemd.user.services.nextcloud-client = {
    Unit = {
      After = ["tray.target" "graphical-session.target"];
    };
  };
}
