{
  config,
  lib,
  pkgs,
  ...
}: {
  home.packages = with pkgs; [
    playerctl
    brightnessctl
  ];

  programs.niri.settings = {
    #    spawn-at-startup = [
    #      {command = ["xwayland-satellite"];}
    #    ];
    #
    #    environment = {
    #      GDK_BACKEND = "x11";
    #      #DISPLAY = ":0";
    #    };

    outputs."eDP-1" = {
      scale = 1.5;
      variable-refresh-rate = true;
    };

    input = {
      keyboard.xkb.layout = "us";
      focus-follows-mouse.enable = true;
      focus-follows-mouse.max-scroll-amount = "0%";
      tablet.map-to-output = "eDP-1";
    };
    hotkey-overlay.skip-at-startup = true;
    prefer-no-csd = true;

    layout = {
      border = {
        active = {
          gradient = {
            to = "#${config.lib.stylix.colors.base03}";
            from = "#ffffff";
          };
        };
      };

      focus-ring.enable = false;
      gaps = 20;
    };

    window-rules = [
      {
        draw-border-with-background = false;
        geometry-corner-radius = let
          r = 12.0;
        in {
          top-left = r;
          top-right = r;
          bottom-left = r;
          bottom-right = r;
        };
        clip-to-geometry = true;
      }
    ];
    binds = {
      "Mod+Shift+Slash".action.show-hotkey-overlay = {};
      "Mod+Return".action.spawn = "kitty";
      "Mod+R".action.spawn = "fuzzel";
      "Mod+N".action.spawn = "networkmanager_dmenu";
      "Mod+B".action.spawn = "dmenu-bluetooth";
      "Mod+P".action.spawn = ["keepmenu" "-a" "{PASSWORD}" "-c" "~/.config/keepmenu.ini"];
      "Mod+U".action.spawn = ["keepmenu" "-a" "{USERNAME}" "-c" "~/.config/keepmenu.ini"];
      "Mod+C".action.spawn = ["~/.local/share/fuzzel_calc"];
      "Mod+F".action.spawn = ["~/.local/share/fuzzel_filebrowser"];

      "Mod+M".action.spawn = ["makoctl" "mode" "-t" "do-not-disturb"];

      "Mod+G".action.spawn = ["env" "DISPLAY=:0" "steam"];

      "XF86AudioRaiseVolume" = {
        allow-when-locked = true;
        action.spawn = ["wpctl" "set-volume" "@DEFAULT_AUDIO_SINK@" "0.01+"];
      };
      "XF86AudioLowerVolume" = {
        allow-when-locked = true;
        action.spawn = ["wpctl" "set-volume" "@DEFAULT_AUDIO_SINK@" "0.01-"];
      };
      "XF86AudioMute" = {
        allow-when-locked = true;
        action.spawn = ["wpctl" "set-mute" "@DEFAULT_AUDIO_SINK@" "toggle"];
      };
      "XF86AudioMicMute" = {
        allow-when-locked = true;
        action.spawn = ["wpctl" "set-mute" "@DEFAULT_AUDIO_SOURCE@" "toggle"];
      };
      "XF86MonBrightnessUp" = {
        allow-when-locked = true;
        action.spawn = ["brightnessctl" "-d" "intel_backlight" "set" "+1%"];
      };
      "XF86MonBrightnessDown" = {
        allow-when-locked = true;
        action.spawn = ["brightnessctl" "-d" "intel_backlight" "set" "1%-"];
      };
      "XF86AudioPlay" = {
        allow-when-locked = true;
        action.spawn = ["playerctl" "play-pause"];
      };
      "XF86AudioPrev" = {
        allow-when-locked = true;
        action.spawn = ["playerctl" "previous"];
      };
      "XF86AudioNext" = {
        allow-when-locked = true;
        action.spawn = ["playerctl" "next"];
      };

      "Mod+Q".action.close-window = {};

      "Mod+Left".action.focus-column-left = {};
      "Mod+Down".action.focus-window-down = {};
      "Mod+Up".action.focus-window-up = {};
      "Mod+Right".action.focus-column-right = {};

      "Mod+Ctrl+Left".action.move-column-left = {};
      "Mod+Ctrl+Down".action.move-window-down = {};
      "Mod+Ctrl+Up".action.move-window-up = {};
      "Mod+Ctrl+Right".action.move-column-right = {};

      "Mod+Shift+Left".action.focus-monitor-left = {};
      "Mod+Shift+Down".action.focus-monitor-down = {};
      "Mod+Shift+Up".action.focus-monitor-up = {};
      "Mod+Shift+Right".action.focus-monitor-right = {};

      "Mod+Shift+Ctrl+Left".action.move-column-to-monitor-left = {};
      "Mod+Shift+Ctrl+Down".action.move-column-to-monitor-down = {};
      "Mod+Shift+Ctrl+Up".action.move-column-to-monitor-up = {};
      "Mod+Shift+Ctrl+Right".action.move-column-to-monitor-right = {};

      "Mod+1".action.focus-workspace = 1;
      "Mod+2".action.focus-workspace = 2;
      "Mod+3".action.focus-workspace = 3;
      "Mod+4".action.focus-workspace = 4;
      "Mod+5".action.focus-workspace = 5;
      "Mod+6".action.focus-workspace = 6;
      "Mod+7".action.focus-workspace = 7;
      "Mod+8".action.focus-workspace = 8;
      "Mod+9".action.focus-workspace = 9;
      "Mod+Ctrl+1".action.move-column-to-workspace = 1;
      "Mod+Ctrl+2".action.move-column-to-workspace = 2;
      "Mod+Ctrl+3".action.move-column-to-workspace = 3;
      "Mod+Ctrl+4".action.move-column-to-workspace = 4;
      "Mod+Ctrl+5".action.move-column-to-workspace = 5;
      "Mod+Ctrl+6".action.move-column-to-workspace = 6;
      "Mod+Ctrl+7".action.move-column-to-workspace = 7;
      "Mod+Ctrl+8".action.move-column-to-workspace = 8;
      "Mod+Ctrl+9".action.move-column-to-workspace = 9;

      "Mod+Shift+F".action.fullscreen-window = {};

      "Mod+Minus".action.set-column-width = "-5%";
      "Mod+Equal".action.set-column-width = "+5%";

      "Print".action.screenshot = {};
      "Ctrl+Print".action.screenshot-screen = {};
      "Alt+Print".action.screenshot-window = {};

      "Mod+Shift+E".action.quit = {};
    };
  };
}
