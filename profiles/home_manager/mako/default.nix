{
  config,
  pkgs,
  ...
}: {
  home.packages = with pkgs; [
    libnotify
  ];

  services.mako = {
    enable = true;
    borderRadius = 6;
    borderSize = 3;
    extraConfig = ''
      [mode=do-not-disturb]
      invisible=1
    '';
  };
}
