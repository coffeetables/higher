{
  config,
  pkgs,
  ...
}: {
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };

  environment.systemPackages = with pkgs; [
    greetd.tuigreet
    nautilus
  ];

  environment.sessionVariables = rec {
    NIXOS_OZONE_WL = "1";
  };

  programs.ydotool.enable = true;

  services.greetd = {
    enable = true;
    settings = let
      tuigreet = "${pkgs.greetd.tuigreet}/bin/tuigreet";
      default_session = {
        command = "${tuigreet} --greeting 'Above the CoffeeTable' --asterisks --remember --remember-user-session";
      };
    in {
      inherit default_session;
    };
  };

  programs.niri.enable = true;
}
