{
  config,
  pkgs,
  lib,
  ...
}: {
  fonts = {
    packages = with pkgs; [
      noto-fonts
      noto-fonts-cjk-sans
      noto-fonts-emoji
      font-awesome
      #nerd-fonts.space-mono

      (nerdfonts.override {fonts = ["SpaceMono"];})
    ];
    fontconfig.defaultFonts = {
      serif = ["Space Mono"];
      sansSerif = ["Space Mono"];
    };
  };

  stylix = {
    enable = true;
    autoEnable = true;
    polarity = "dark";
    base16Scheme = "${pkgs.base16-schemes}/share/themes/silk-dark.yaml";
    image = config.lib.stylix.pixel "base00";
    opacity = {
      applications = 1.0;
      desktop = 0.5;
      popups = 0.8;
      terminal = 0.8;
    };
  };

  #stylix.targets.kubecolor.enable = lib.mkForce false;

  stylix.targets.grub.useImage = true;
}
