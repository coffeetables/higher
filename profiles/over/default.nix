{
  self,
  config,
  pkgs,
  modulesPath,
  ...
}: {
  imports = [(modulesPath + "/installer/scan/not-detected.nix")] ++ self.suites.over;

  boot = {
    initrd.availableKernelModules = ["xhci_pci" "thunderbolt" "nvme" "usb_storage" "sd_mod"];
    kernelModules = ["kvm-intel"];
  };

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/0d5de940-a111-4f6d-83f7-0094de1819da";
    fsType = "ext4";
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/CBE8-2B5E";
    fsType = "vfat";
  };

  swapDevices = [
    {device = "/swapfile";}
  ];

  system.stateVersion = "24.05";
}
