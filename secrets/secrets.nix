let
  rightleftspinOver = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFjr4OUkejq64qzvQpnw1N7Hk2Cvo9y003mv91T0ENjK";
  rightleftspinServer = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPpvN8hQxlPdsoqE13eZ5DKgmvhS1u+IfYNBDv0X3J8V";
  pachum = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJKMdikyBtQhYP6AiSrWQk2u9/ShitzugNZC5ForUPP4";

  over = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICevjl3yxc4PXu0EhwKAypf7o8lVwlQfj81k3CajL5my";

  users = [rightleftspinOver];
  systems = [over];
in {
  "rightleftspin.age".publicKeys = users ++ systems;
  "keepass_main.age".publicKeys = users ++ systems;
}
