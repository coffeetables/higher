# To build this, hop into the devshell and run
# colmena apply-local --sudo
{
  description = "Higher Coffee Table Configuration Files";

  inputs = {
    nixos.url = "nixpkgs/nixos-24.11";
    nixpkgs.follows = "nixos";
    nixpkgs-stable.url = "nixpkgs/nixos-24.11";
    nur.url = "nur";

    flake-parts.url = "github:hercules-ci/flake-parts";
    haumea.url = "github:nix-community/haumea";

    home.url = "github:nix-community/home-manager/release-24.11";
    agenix.url = "github:ryantm/agenix";
    colmena.url = "github:zhaofengli/colmena";

    devshell.url = "github:numtide/devshell";
    treefmt-nix.url = "github:numtide/treefmt-nix";

    nixos-hardware.url = "github:NixOS/nixos-hardware/master";

    scientific-fhs.url = "github:olynch/scientific-fhs";
    nixgl.url = "github:nix-community/nixGL";

    niri.url = "github:sodiboo/niri-flake";
    stylix.url = "github:danth/stylix/release-24.11";

    nix-doom-emacs-unstraightened.url = "github:marienz/nix-doom-emacs-unstraightened";
    nix-doom-emacs-unstraightened.inputs.nixpkgs.follows = "";
  };

  outputs = {
    self,
    nixos,
    nixpkgs,
    nixpkgs-stable,
    nur,
    flake-parts,
    nixgl,
    haumea,
    agenix,
    home,
    nixos-hardware,
    treefmt-nix,
    devshell,
    scientific-fhs,
    niri,
    stylix,
    ...
  } @ inputs:
    flake-parts.lib.mkFlake {inherit inputs;} ({getSystem, ...}: {
      imports = [
        devshell.flakeModule
        treefmt-nix.flakeModule
      ];

      systems = ["aarch64-linux" "x86_64-linux"];

      perSystem = {
        inputs',
        pkgs,
        ...
      }: {
        treefmt = {
          programs.alejandra.enable = true;
          flakeFormatter = true;
          projectRootFile = "flake.nix";
        };

        devshells.default = {pkgs, ...}: {
          commands = [
            {package = inputs'.agenix.packages.default;}
            {package = inputs'.colmena.packages.colmena;}
          ];
        };
      };

      flake.profiles = haumea.lib.load {
        src = ./profiles;
        loader = haumea.lib.loaders.path;
      };

      flake.suites = let
        profiles = self.profiles;
      in {
        hm = with profiles.home_manager;
          map (x: x.default)
          [
            core
            emacs
            firefox
            fuzzel
            kdeconnect
            kitty
            mako
            neovim
            nextcloud
            profiles.home_manager.niri
            my-scientific
            swaybg
            waybar
          ];

        over = with profiles;
          map (x: x.default)
          [
            boot.efi
            core
            gaming
            networking
            profiles.niri
            pkgs
            power
            style
            users.rightleftspin
            xdg
          ];
      };

      flake.colmena = {
        meta = {
          nixpkgs = import nixpkgs {
            system = "x86_64-linux";
            config.allowUnfree = true;
            overlays = [
              niri.overlays.niri
              inputs.nur.overlays.default
              nixgl.overlay
              (
                final: prev: {
                  languagetool = prev.languagetool.overrideAttrs (finalAttrs: previousAttrs: {
                    src = prev.fetchzip {
                      url = "https://www.languagetool.org/download/LanguageTool-5.9.zip";
                      sha256 = "sha256-x4xGgYeMi7KbD2WGHOd/ixmZ+5EY5g6CLd7/CBYldNQ=";
                    };
                  });

                  fuzzel_as_dmenu = prev.linkFarm "dmenu" [
                    {
                      name = "bin/dmenu";
                      path = "${prev.fuzzel}/bin/fuzzel";
                    }
                  ];

                  fuzzel_as_rofi = prev.linkFarm "rofi" [
                    {
                      name = "bin/rofi";
                      path = "${prev.fuzzel}/bin/fuzzel";
                    }
                  ];
                  gamescope = prev.gamescope.overrideAttrs (final: rec {
                    version = "3.14.29";
                    src = prev.fetchFromGitHub {
                      owner = "ValveSoftware";
                      repo = "gamescope";
                      rev = "refs/tags/${version}";
                      fetchSubmodules = true;
                      hash = "sha256-q3HEbFqUeNczKYUlou+quxawCTjpM5JNLrML84tZVYE=";
                    };
                  });
                }
              )
            ];
          };
          specialArgs = {
            inherit self;
            pkgs-stable = import nixpkgs-stable {
              system = "x86_64-linux";
              config.allowUnfree = true;
            };
          };
        };
        defaults = {pkgs, ...}: let
          moduleArgs = {
            inherit
              ((getSystem pkgs.system).allModuleArgs)
              self'
              inputs'
              ;
          };
        in {
          imports = [
            home.nixosModules.home-manager
            agenix.nixosModules.age
            stylix.nixosModules.stylix
            ({pkgs, ...}: {
              imports = [niri.nixosModules.niri];
              programs.niri.package = pkgs.niri-stable;
            })
          ];
          _module.args = moduleArgs;
          home-manager = {
            useGlobalPkgs = true;
            useUserPackages = true;
            sharedModules = [
              {_module.args = moduleArgs;}
              inputs.nix-doom-emacs-unstraightened.hmModule
            ];
            extraSpecialArgs = {inherit scientific-fhs;};
          };
        };

        OverTheCoffeeTable = {
          networking.hostName = "OverTheCoffeeTable";
          deployment.allowLocalDeployment = true;
          deployment.targetHost = null;
          imports = [
            self.profiles.over.default
            nixos-hardware.nixosModules.framework-11th-gen-intel
          ];
        };
      };
    });
}
